package wachi.survey.flavorssurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import wachi.survey.flavorssurvey.BuildConfig;
import wachi.survey.flavorssurvey.R;

public class MainActivity extends BaseActivity {
    private TextView textView;
    private RecyclerView recyclerView;

    @Override
    protected int getResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void findView() {
        textView = findViewById(R.id.title);
    }

    @Override
    protected void setupViewComponent() {
        textView.setText("--- " + BuildConfig.SIT_HOST + " ---");
    }
}

package wachi.survey.flavorssurvey.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import wachi.survey.flavorssurvey.R;

abstract public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResId());
        findView();
        setupViewComponent();
    }

    abstract protected int getResId();
    abstract protected void findView();
    abstract protected void setupViewComponent();

}
